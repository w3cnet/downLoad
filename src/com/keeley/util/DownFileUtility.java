package com.keeley.util;

import org.eclipse.swt.widgets.Text;

import com.jfinal.plugin.activerecord.Db;

/**
 * Created with IntelliJ IDEA. User: Administrator Date: 14-7-29 Time: 下午2:14 To
 * change this template use File | Settings | File Templates.
 */
public class DownFileUtility {
	public DownFileUtility() {
	}

	/**
	 * 休眠时长
	 * 
	 * @param nSecond
	 */
	public static void sleep(int nSecond) {
		try {
			Thread.sleep(nSecond);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 打印日志信息
	 * 
	 * @param sMsg
	 */
	public static void log(int downid, String sMsg) {
		// \r\n
		// System.out.println("打印日志入库"+downid);
		System.err.println(sMsg);
		String l = Db.findFirst("select * from down where id=?;", downid).getStr("logs");
		l = null == l ? "" : (l + "\r\n");
		Db.update("update down   set logs=?  where  id=?;", l + sMsg, downid);
	}

	public static void log(String sMsg) {
		// \r\n

		System.err.println(sMsg);
	}

	/**
	 * 打印日志信息
	 * 
	 * @param sMsg
	 */
	public static void log(int sMsg) {
		System.err.println(sMsg);
	}
}
